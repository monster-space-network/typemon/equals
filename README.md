# Equals 

[![npm-version](https://img.shields.io/npm/v/@typemon/equals.svg)](https://www.npmjs.com/package/@typemon/equals)
[![npm-downloads](https://img.shields.io/npm/dt/@typemon/equals.svg)](https://www.npmjs.com/package/@typemon/equals)

Strict and Deep



# Usage

```
$ npm install @typemon/equals
```

```typescript
import { equal, notEqaul } from '@typemon/equals';
```

## Equal

```typescript
equal(NaN, NaN); // true
equal(-0, +0);   // false

equal([], [,]);  // false
equal([0, { foo: 1 }, 2], [0, { foo: 1 }, 2]);     // true
equal([0, { foo: 1 }, 2], [0, { foo: 'bar' }, 2]); // false

equal({ foo: { bar: 1 } }, { foo: { bar: 1 } });   // true
equal({ foo: { bar: 1 } }, { foo: { bar: '1' } }); // false
```
```typescript
const a: Date = new Date();
const b: Date = new Date();
const c: Date = new Date(Date.now() + 1);

equal(a, b); // true
equal(a, c); // false
```

## Not Equal

```typescript
!equal(a, b);
notEqual(a, b);
```
