//
//
//
export function equal(a: any, b: any): boolean {
    if (a === b) {
        return a !== 0 || 1 / a === 1 / b; // -0 != +0
    }

    if (typeof a === 'object' && typeof b === 'object') {
        if (a === null || b === null) {
            return false;
        }

        const aIsDate: boolean = a instanceof Date;
        const bIsDate: boolean = b instanceof Date;

        if (aIsDate !== bIsDate) {
            return false;
        }

        if (aIsDate && bIsDate) {
            return a.getTime() === b.getTime();
        }

        const aIsArray: boolean = a instanceof Array;
        const bIsArray: boolean = b instanceof Array;

        if (aIsArray !== bIsArray) {
            return false;
        }

        if (aIsArray && bIsArray && a.length !== b.length) {
            return false;
        }

        const keys: string[] = Object.keys(a);

        if (keys.length !== Object.keys(b).length) {
            return false;
        }

        const bObject: Object = new Object(b);

        for (let index: number = 0, key: string = keys[index]; index < keys.length; key = keys[++index]) {
            if (!bObject.hasOwnProperty(key)) {
                return false;
            }

            if (!equal(a[key], b[key])) {
                return false;
            }
        }

        return true;
    }

    return a !== a && b !== b; // NaN == NaN
}

export function notEqual(a: any, b: any): boolean {
    return !equal(a, b);
}
